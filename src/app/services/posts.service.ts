import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Post } from '../models/Post'; 

const httpOptions = {
  headers : new HttpHeaders({'Content-Type':'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts?: Post[];
  url: string = "https://jsonplaceholder.typicode.com/posts";

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]> {
      return this.http.get<Post[]>(this.url);
  }

  addPost(post: Post): Observable<Post>{
    return this.http.post<Post>(this.url, post, httpOptions);
  }

  getPostById(id:number): Observable<Post> {
    const urlById = `${this.url}/${id}`;
    return this.http.get<Post>(urlById);
  }

  updatePost(post:Post): Observable<Post> {
    const urlById = `${this.url}/${post.id}`;
    return this.http.put<Post>(urlById, post, httpOptions);
  }

  deletePost(post: Post | number):Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const urlById = `${this.url}/${id}`;
    return this.http.delete<Post>(urlById);
  }
}
