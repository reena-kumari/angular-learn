import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { Employee } from '../models/Employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees!: Employee[];
  data!: Observable<any>;
  constructor() { 
    this.employees = [
      {
        id: 101,
        name: "Sam",
        email: "sam@gmail.com",
        phone: 12345678,
        hide: true
      },
      {
        id: 102,
        name: "Kim",
        email: "kim@gmail.com",
        phone: 12345678,
        hide: true
      },
      {
        id: 103,
        name: "John",
        email: "john@gmail.com",
        phone: 12345678,
        hide: true
      },
      {
        id: 104,
        name: "David",
        email: "david@gmail.com",
        phone: 12345678,
        hide: true
      }
    ];
  }

  getData(){
    this.data = new Observable(observer=>{
      setTimeout(()=> {
        observer.next(1);
      },1000);
      setTimeout(()=> {
        observer.next(2);
      },2000);
      setTimeout(()=> {
        observer.next(3);
      },3000);
      setTimeout(()=> {
        observer.next(4);
      },4000);
    });
    return this.data;
  }

  getEmployee(): Observable<Employee[]>{
    return of(this.employees);
  }

  addEmployee(employee: Employee){
    this.employees.unshift(employee);
  }
}
