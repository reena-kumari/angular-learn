export interface User{
    firstName: string;
    lastName: string;
    age: number;
    role: string;
    image?: string;
    isActive?:boolean;
    balance?: number;
    registered?: Date;
} 