import {Component, OnInit} from '@angular/core';

import { User } from '../../models/User';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
    // template: '<h2>John Doe</h2>'
    // styles: [`
    //     h2 {
    //         color: blue
    //     }
    // `]
})

export class UserComponent implements OnInit {
   user!: User;
  
//    numberArray: number[];
//    mixedArray: any[];
//    myTuple: [number, string, boolean];

    //methods
    constructor(){
       
        // this.numberArray = [2,3]; 
        // this.mixedArray = ['hi',3];
        // this.myTuple = [2,'hi',true];
    }

    ngOnInit() {
        this.user = {
            firstName: 'John',
            lastName:'Robbert',
            age: 30,
            role:'manager'
        }
        
      }

    sayHello(){
        console.log(`hello ${this.user.firstName}`);      
    }

    hasBirthday(): number{
        return this.user.age +=1;
    }
}

