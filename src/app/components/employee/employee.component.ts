import { Component, OnInit, ViewChild } from '@angular/core';
import { Employee } from '../../models/Employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees!: Employee[];
  employee : Employee = {
    id: 0,
    name: '',
    email:'',
    phone: 0
  }

  showUserForm: boolean;
  @ViewChild('empForm') form: any;
  data: any;

  constructor(private empService: EmployeeService) {
    this.showUserForm = true;
   }

  ngOnInit(): void {
    //suscribing for observable
    //used for async calls
    this.empService.getData().subscribe(data => {
      console.log(data);    
    });

    this.empService.getEmployee().subscribe(data=>{
      this.employees = data;
    });
  }

  // adduser(){
  //   this.employees.unshift(this.employee);
  //   this.employee = {
  //     id: 0,
  //     name: '',
  //     email:'',
  //     phone: 0
  //   }
  // }

  toggleHide(e: Employee){
    e.hide = !e.hide;
  }

  onSubmit(){
    if(!this.form.valid){
      console.log('not valid');   
    } else {
      this.empService.addEmployee(this.form.value);
      this.form.reset();
    }
  }

}
