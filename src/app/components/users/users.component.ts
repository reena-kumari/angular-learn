import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users!: User[];

  showExtended: boolean = true;
  enableAdd:boolean = true;
  currentClasses = {};
  currentStyles = {};

  constructor() {
    console.log('const...');
    //depency injection
   }

  ngOnInit(): void {
    this.users = [
       {
        firstName: 'Kevin',
        lastName:'Robbert',
        age: 70,
        role: 'Manager',
        image: 'http://lorempixel.com/600/600/people/3',
        isActive: false,
        balance: 150,
        registered: new Date("01/02/2018 08:30:33")
      },
      {
        firstName: 'John',
        lastName:'Erik',
        age: 35,
        role: 'Developer',
        image: 'http://lorempixel.com/600/600/people/2',
        isActive: true,
        balance: 1200,
        registered: new Date("08/02/2019 10:30:00")
      },
      {
        firstName: 'Erik',
        lastName:'Doe',
        age: 25,
        role: 'Developer',
        image: 'http://lorempixel.com/600/600/people/1',
        isActive: true,
        balance: 140,
        registered: new Date("01/02/2020 08:30:33")
      }
    ]
    this.setCurrentClasses();
    this.setCurrentStyles();
    console.log('init...');
   // this.showExtended = false;
  }

  setCurrentClasses(){
    this.currentClasses = {
      'btn-success':this.enableAdd,
      'big-text': this.showExtended
    }
  }

  setCurrentStyles(){
    console.log('setting styles');
    this.currentStyles = {
      'padding-top': this.showExtended ?'0':'60px'
    }
  }

}
