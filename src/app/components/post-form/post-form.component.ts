import { Component, EventEmitter, OnInit,Output, Input } from '@angular/core';

import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/Post';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() newPost = new EventEmitter<Post>();
  @Output() updatedPost = new EventEmitter<Post>();
  @Input() currentPost!: Post;
  @Input() isEdit!: boolean;

  constructor(private postService: PostsService) { }

  ngOnInit(): void {
  }

  addPost(title: string, body: string){
    console.log(title, body);
    if(!title||!body){
      alert('please add post');
    } else {
      this.postService.addPost({userId: 1020, title, body} as Post).subscribe(post=>{
        console.log(post); 
        this.newPost.emit(post);     
      });
    }

  }

  updatePost(){
    this.postService.updatePost(this.currentPost).subscribe(post=>{
      console.log(post); 
      this.isEdit = false;
      this.updatedPost.emit(post);
    });
    
  }

}
