import { Component, OnInit } from '@angular/core';

import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/Post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts?: Post[];
  //this is going to attach to form, so default value required
  currentPost: Post= {
    title:'',
    body:'',
    id:0
  };

  isEdit: boolean = false;

  constructor(private postService: PostsService) { }

  ngOnInit(): void {

    this.postService.getPosts().subscribe(data => {
      console.log(data);     
      this.posts = data;
    });
    
  }

  onNewPost(post: Post){
    this.posts?.unshift(post);
    console.log(post);
  }

  editPost(post: Post){
    this.currentPost = post;
    this.isEdit = true;
  }

  onUpdatePost(post:Post){
    this.isEdit = false;
    this.posts?.forEach((p, index)=>{
      if(post.id==p.id){
        this.posts?.splice(index, 1);
        this.posts?.unshift(post);
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        }
      }
    })
  }

  deletePost(post: Post){
    if(confirm('Are you sure?')){
      this.postService.deletePost(post.id).subscribe(data => {
        console.log(data); 
        this.posts?.forEach((p, index)=>{
          if(post.id==p.id){
            this.posts?.splice(index, 1);
          }
        })    
      });
    }  
  }

}
